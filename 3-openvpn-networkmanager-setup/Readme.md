---
SETUP OPENVPN SERVER LOCATION, WITH NETWORK-MANAGER. GTK-GUI [for me that's surfshark]
---

- Rightclick on network-manager-applet systray icon. Go to edit connections.

---
#### Import saved VPN config. [.ovpn] file.
---

![Schermafdruk_2022-08-15_23-04-24](/uploads/a3948983bf6076ffc2e79aeac1099ac5/Schermafdruk_2022-08-15_23-04-24.png)

![Schermafdruk_2022-08-15_23-29-23](/uploads/4da95716f8ff76a0ac8bc42c12361ec0/Schermafdruk_2022-08-15_23-29-23.png)

---

#### Go to VPN settings : Fill in login details. _Store password for all users._

- When choose ``store password for all users``, the config files will be located at ``/etc/NetworkManager/system-connections/``
-  For ``store only for this user``, you need an application that can ask for the secrets and store them somewhere. It's up to that application, for example ``nm-applet`` stored the secret to the user's keyring.

---

- Username: The user name created in Step 1. DO NOT use your regular  VPN-provider user name!
- Password: The password created in Step 1. DO NOT use your regular VPN-provider password! 
- Get Details from your vpn-provider website

![Schermafdruk_2022-08-15_22-47-45](/uploads/62be285d79df16e75308ef1c824642aa/Schermafdruk_2022-08-15_22-47-45.png)

---

#### Go to Advanced to add extra certificate. (option)

---

![Schermafdruk_2022-08-15_23-05-28](/uploads/de2ece8e8c085133a3489db46df30bed/Schermafdruk_2022-08-15_23-05-28.png)

---

#### Add certificate ::

#### ~/.cert  

---

![g933](/uploads/66b2bf1a23d6b724a92b146f55a509c5/g933.png)

---

#### Disable Ipv6 : at least for Surfshark Vpn. They don't use it. And it can be in the way, they say.

---

![Schermafdruk_2022-08-15_22-48-56](/uploads/abe5c3951def387b13b54b444aec7d3c/Schermafdruk_2022-08-15_22-48-56.png)

---

#### FINAL - edit your ``wifi and/or eth0`` settings. Enable auto connect, and choose your fav location.

---

![g933hh](/uploads/76bc3836732123b453b6f028f5a26b38/g933hh.png)

---

##### Choose your server location

![Schermafdruk_2022-08-16_10-23-28](/uploads/5ea129481dbd87e7ffc371ddf6922d1c/Schermafdruk_2022-08-16_10-23-28.png)


---

[__CHECK IP__ ](https://www.iplocation.net/) : iplocation.net

---
_command_ : restart network-manager service

``sudo service network-manager restart``

