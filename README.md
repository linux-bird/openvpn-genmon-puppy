## OPENVPN tools (for free and paid services)

what's in this git.


    Main folder - Add check-vpn-gen, up/down status to xfce4-panel. Click function, speedtest-cli. Info balloon, IPGEO output.
    
    openvpn-connect folder - Setup openvpn-connect manualy, with start and stop scripts. (all desktops)

    3-openvpn-networkmanager-setup - How to setup .ovpn with gnome-network-manager.

    4-openvpn3_frontend_installation_debian_ubuntu - How to install / use openvpn3 client.

# __openvpn-genmon-xfce4-panel__ :::

There are two scripts in this folder ;

    check-vpn-gen
    speedtest-cli.sh

These scripts are meant for the xfce4-panel genmon plugin on xfce4 desktop. Or another setup using xfce4-panel.


##### DEPS 

> ``xfce4-panel, xfce4-genmonitor, speedtest-cli, openvpn, zenity, netstat, ifconfig, hwinfo,``

> ``ipgeo`` [Github archived : linux :](https://github.com/jakewmeyer/Geo) . I use this when running on debian based systems. Not maintained for a long time. But it still works. [Arch](https://aur.archlinux.org/packages/ipgeo-git) has more updated ipgeo.



## LOCATION : check-vpn tray  system wide . . . [inspired by](https://github.com/xtonousou/xfce4-genmon-scripts)

> __Create a Genmon on the panel and setup__

``/usr/local/bin/check-vpn-gen``  - use this path in Genmon.

``/usr/local/bin/speedtest-notif.sh``

``~/.icons ``        

#### [Edit -> check-vpn-gen script to change icon to yours]


#### When clicking tray-icon speedtest-cli runs in zenity window


---


## Go to [openvpn-connect Gitlab muzlabz](https://gitlab.com/muzlabz/openvpn-genmon-puppy/-/tree/main/openvpn-connect) folder . . .
## Setup openvpn and create Start and Stop VPN script.

_Bash scripts to launch .openvpn connection - HowTo in folder._


