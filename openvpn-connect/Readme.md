### ABOUT 


##### This is a simple VPN implementation using OpenVPN and some scripts and other files intended for use with some free VPN providers but it can also be adapted for use with other (paid-for) VPN providers.

##### vpn-onoff  [linux]    :::: example surfshark as vpn-service


##### _--- credits vpn-onoff -- OscarTalks puppylinux ---_





---
#### CONFIGURATION  ::: _Deps -> yad, OpenVPN, gtkdialog._

---

__OpenVPN config__ files go in this directory  [create the Dir if not exist].

``/etc/vpn-onoff/``.

Like, 

`` /etc/vpn-onoff/vpnconfig ``

These config files usually have the extension .ovpn.

- Remove/Backup ``vpnconfig``

- Replace it with your ``.ovpn`` file.

- Rename ``.ovpn`` file with ``vpnconfig`` and put it in /etc/vpn-onoff/.

_(this ``vpnconfig`` file is setup for surfshark nl-tcp)_

---
#### CHECK vpnconfig file
---

Edit ``vpnconfig`` with credentials ::

- Add the following line to ``vpnconfig``. _See current vpnconfig as example._

``auth-user-pass  /etc/vpn-onoff/vpnpass ``

---
#### Now edit vpnpass file and place it in /etc/vpn-onoff/.
---

The ``vpnpass`` file is a simple text file with the ``username`` on the __First Line__ and the ``password`` on the __Second Line__, nothing else.

> _For surfshark - go to the website - to manual setup / router - and get inlog details for extended applications._

![surshark-router](/uploads/0584417e5395cdb77f1a184ee391472c/surshark-router.png)

---
#### location vpn-start and vpn-stop scripts [system-wide].
---

``/usr/local/bin/vpn-start``

``/usr/local/bin/vpn-stop``

``/usr/share/applications/vpn-start.desktop``

``/usr/share/applications/vpn-stop.desktop``

---

---
#### USE : 
---

![Schermafdruk_2022-08-14_23-19-55](/uploads/566b036d34003279df44408addd8b8ae/Schermafdruk_2022-08-14_23-19-55.png)

![Schermafdruk_2022-08-14_23-20-12](/uploads/0ef596b2f97c96b3a922f09899510d02/Schermafdruk_2022-08-14_23-20-12.png)



For automatic connect at boot add ``VPN-START`` script to autostart session.

---
#### [Inspired by ](https://forum.puppylinux.com/viewtopic.php?p=1349&hilit=VPN+OnOff#p1349)

#### [Download page vpn-onoff for puppylinux](https://smokey01.com/OscarTalks/) : Puppylinux .pet files.
---

_ps:: all surfshark needed files are zipped._
